import base64
import socket
from urllib.request import urlopen, Request

DUMMY_ADDRESS = ('8.8.8.8', 1)

USER_AGENT = 'User-Agent'
AUTHORIZATION = 'Authorization'

URL_FORMAT = 'https://domains.google.com/nic/update?hostname=%s&myip=%s'
AUTHORIZATION_FORMAT = '%s:%s'
AUTHORIZATION_HEADER_FORMAT = 'Basic %s'

GOOD_RESPONSES = ['good', 'nochg']
NO_CHANGE = 'nochg'

RESPONSE_MESSAGES = {
    'good': 'The DNS update was successful.',
    'nochg': 'The IP was already up to date.',
    'nohost': 'Update Failed -> The hostname does not exist, or does not '
              'have Dynamic DNS enabled.',
    'badauth': 'Update Failed -> The username / password combination is not '
               'valid for the specified host.',
    'notfqd': 'Update Failed -> The supplied hostname is not a valid '
              'fully-qualified domain name.',
    'badagent': 'Update Failed -> The supplied user agent is not good.',
    'abuse': 'Update Failed -> Dynamic DNS access for the hostname has been '
             'blocked due to failure to interpret previous responses '
             'correctly.',
    '911': 'An error happened on Google\'s end. Wait 5 minutes and retry.'
}


def _get_server_ip() -> str:
    """Returns the server's ip"""
    # create dummy UDP socket to retrieve server ip
    # (the socket doesnt send anything)
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(DUMMY_ADDRESS)
    return s.getsockname()[0]


def _dns_correct(domain: str) -> bool:
    """
    Checks if the IP for the domain is the same as the ip of the server

    :return: Weather the IP found by DNS for the domain matches the server's IP
    """
    dns_ip = socket.gethostbyname(domain)
    return dns_ip == _get_server_ip()


def update_dns(domain: str,
               username: str,
               password: str,
               user_agent: str) -> (bool, str):
    """
    Updates the dynamic DNS record for the domain

    :param domain: The domain the DNS will be updated for
    :param username: The Google Domains dynamic DNS username
    :param password: The Google Domains dynamic DNS password
    :param user_agent: A user agent to use in the request
    :return: a tuple containing a bool that indicates if the update was
    successful, and a str that contains the response message
    """
    # check if there's a need to update the dns
    if _dns_correct(domain):
        return True, RESPONSE_MESSAGES[NO_CHANGE]

    # prepare the authorization
    authorization = base64.b64encode(
        (AUTHORIZATION_FORMAT % (username, password)).encode('utf-8'))
    authorization_header = \
        AUTHORIZATION_HEADER_FORMAT % authorization.decode('utf-8')

    # build the request
    url = URL_FORMAT % (domain, _get_server_ip())
    request = Request(url, headers={USER_AGENT: user_agent,
                                    AUTHORIZATION: authorization_header})

    # execute the request read the response
    response = urlopen(request).read().decode('utf-8')
    response_code = response.split()[0]
    return response_code in GOOD_RESPONSES, RESPONSE_MESSAGES[response_code]
