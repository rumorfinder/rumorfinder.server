import json
import os
from uuid import UUID

from jsonschema import validate, ValidationError

from json_consts import *
from preferences_vector import PreferencesVector


def _load_schema() -> str:
    """loads the request json schema from file"""
    # the file is in the same working directory as the request.py
    # file, so we locate the current directory and append the
    # file name to the current directory to get the full file path
    file_name = "%s\\json-data-schema.json" % \
                os.path.dirname(os.path.abspath(__file__))
    with open(file_name) as f:
        return f.read()


def _validate_uuid(uuid: str) -> bool:
    """checks if the given uuid is a valid v4 UUID"""
    try:
        correct_uuid = UUID(uuid.lower(), version=4)
    except ValueError:
        return False

    return uuid.lower() == str(correct_uuid).lower()


class Request:
    """represents a client request"""
    _request_schema = _load_schema()

    def __init__(self, json_: str):
        self._json = json_

        # load the schema
        self._schema = json.loads(Request._request_schema)

    def is_valid(self) -> bool:
        """
        Check if the request body is valid.
        both the format and given values are checked.
        :return: a bool indicating if the request is valid
        """
        # make sure the format for the request body is correct
        try:
            request_body = json.loads(self._json)
            validate(request_body, self._schema)
        except (ValidationError, json.JSONDecodeError):
            return False

        # validate the id parameter
        if not _validate_uuid(request_body[ID_LOCATION]):
            return False

        preferences = request_body[PREFERENCES_LOCATION]
        search_history = preferences[SEARCH_HISTORY_LOCATION]
        followed_products = preferences[FOLLOWED_PRODUCTS_LOCATION]
        declined_recommendations = \
            preferences[DECLINED_RECOMMENDATIONS_LOCATION]
        entries = search_history + followed_products + declined_recommendations
        checked_entries = []

        # make sure there are no duplicates
        for entry in entries:
            e = str(entry).lower()
            if e in checked_entries:
                return False
            checked_entries.append(e)

        return True

    def parse(self) -> (UUID, PreferencesVector):
        """
        parses the json request body

        :return: a tuple containing the parsed UUID and parsed preferences
        vector
        """
        parsed_json = json.loads(self._json)
        uuid = parsed_json[ID_LOCATION]
        preferences = parsed_json[PREFERENCES_LOCATION]

        # parameterize the keys' names
        for key in preferences.keys():
            value = preferences[key]
            del preferences[key]
            key = key.replace(' ', '_')
            preferences[key] = value

        return UUID(uuid, version=4), PreferencesVector(**preferences)
