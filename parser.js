'use strict';

const Mercury =  require('@postlight/mercury-parser');
const struct = require('structjs');
const net = require('net');
const argv = require('yargs-parser')(process.argv.slice(2));

const {
    _ : [serverPort]
} = argv;

((port) => {
    if (!port){
        console.log('\n\
        Web Parser\n\n\
            The Parser extracts semantic content from any url.\n\
            Pass urls through tcp messages to the passed port.\n\n\
        Usage:\n\
        \n\
            node parser.js port-to-listen-on\n\
        ');
        return;
    }
    
    const p = parseInt(port, 10);
    if (isNaN(p)){
        console.error("Passed port is not a number. please enter a valid port.");
        return;
    };

    const server = net.createServer((clientSocket) => {
        console.log('got client');
        clientSocket.on('error', (err) => {console.error(err)}) // we ignore client errors
        clientSocket.on('data', async (data) => {
            console.log('got data');
            var response = '';
            try {
                const url = data.toString();
                const result = await Mercury.parse(url, {contentType : 'html',});
                
                response = JSON.stringify(result, null, 2);
            } catch(err){
                response = err.toString();
            }
            const resposnseLength = struct('>I').pack(response.length);

            clientSocket.write(Buffer.from(resposnseLength));
            clientSocket.write(response);
        });
    });

    server.on('error', (err) => {
        console.error(err);
        throw err;
    });

    server.listen(p, '127.0.0.1', () => {
        console.log('server bound');
    });

})(serverPort);









