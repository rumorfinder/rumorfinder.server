# RumorFinder Server
This repository contains code for a backend server used by the [Rumorfinder iOS app](https://gitlab.com/galschwietzer/rumorfinder).
The server contains two functions, a web parser and a recommender system, that are written in python (and a little js).

### [Documentation for the project.](https://galschwietzer.gitlab.io/rumorfinder.server)
