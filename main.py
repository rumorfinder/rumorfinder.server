import atexit
import ssl
import sys

import config
from dns import update_dns
from http_request_handler import *


def enable_https(server: ThreadingHTTPServer) -> None:
    """enables https by wrapping the server's socket with an SSL context"""
    # create the context and load the certificate
    context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    context.load_cert_chain(**config.CERT_CHAIN)

    # wrap the socket
    server.socket = context.wrap_socket(server.socket, server_side=True)


def main():
    """Main program for the server"""
    print("Connecting to db...")
    db.connect(config.DB_CREDENTIALS)
    print("Connected to db")

    print("Starting parser processes...")
    try:
        web_parser.start(**config.PARSER_PARAMETERS)
    except TimeoutError:
        sys.exit("Could not start the web parser")
    print("Started parser processes")

    # registering the db.close() and web_parser.stop() methods to be called
    # when the server exits
    atexit.register(web_parser.stop)
    atexit.register(db.close)

    print("Updating DNS records...")
    successful, message = update_dns(**config.DNS)
    if successful:
        print("Updated DNS records to point to current IP")
    else:
        sys.exit(message)

    with ThreadingHTTPServer(config.ADDRESS, HTTPRequestHandler) as server:
        print("Enabling HTTPS...")
        enable_https(server)
        print("Certificate was loaded and HTTPS in enabled")

        print("Running server...")
        print("Listening to %s on port %s" % config.ADDRESS)
        server.serve_forever()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
