# constants for json requests' body

ID_LOCATION = 'id'

PREFERENCES_LOCATION = 'preferences'

SEARCH_HISTORY_LOCATION = 'search history'

FOLLOWED_PRODUCTS_LOCATION = 'followed products'

DECLINED_RECOMMENDATIONS_LOCATION = 'declined recommendations'
