from typing import Sequence
from uuid import UUID

import psycopg2

from preferences_vector import PreferencesVector

_SELECT_QUERY = "SELECT preferences_vector::text FROM user_preferences " \
                "WHERE id != %s"

_UPSERT_QUERY = "INSERT INTO user_preferences (id, preferences_vector) " \
                "VALUES(%s, %s) " \
                "ON CONFLICT (id) DO UPDATE " \
                "SET preferences_vector = EXCLUDED.preferences_vector"


def connect(db_credentials: dict) -> None:
    """
    connects the module to the database

    :param db_credentials: the credentials to use when connecting to the db
    """
    # we use two connections so we can use named cursors
    global _insertion_connection
    global _selection_connection

    _insertion_connection = psycopg2.connect(**db_credentials)
    _insertion_connection.autocommit = True

    _selection_connection = psycopg2.connect(**db_credentials)
    _selection_connection.autocommit = True


def close() -> None:
    """closes the connection"""
    _insertion_connection.close()
    _selection_connection.close()


def select_other_neighbours(id: UUID) -> Sequence[PreferencesVector]:
    """
    returns all the neighbours for a given id

    :param id: the id of the user for which the neighbours will be returned
    :return: a sequence of the preferences vector of each found neighbour
    """
    # we use a named cursor to avoid pulling the entire table into memory
    with _selection_connection.cursor(name=str(id), withhold=True) as cursor:
        cursor.execute(_SELECT_QUERY, (str(id),))
        for row in cursor:
            yield PreferencesVector.from_json(row[0])


def upsert_user(id: UUID, preferences_vector: PreferencesVector) -> None:
    """
    Inserts or updates(upsert) the given user into the db

    :param id: the id to be inserted
    :param preferences_vector: the preferences vector to be inserted
    """
    with _insertion_connection.cursor() as cursor:
        cursor.execute(_UPSERT_QUERY, (str(id), preferences_vector.json()))
