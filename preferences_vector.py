import json
from math import sqrt
from typing import List

from vector_consts import *


class PreferencesVector:
    """Represents a multi dimensional vector that stores rating by a user
    for different products"""

    @classmethod
    def from_json(cls, json_: str) -> 'PreferencesVector':
        """deserializes the vector from json"""
        v = cls()
        v._vector = json.loads(json_)
        v.rated_products = [k.lower() for k in v._vector.keys()]
        return v

    def __init__(self, search_history: List[str] = list(),
                 followed_products: List[str] = list(),
                 declined_recommendations: List[str] = list()):
        """creates a new vector from the given preferences"""
        # fill the vector with values
        self._vector = {}
        for searched_product in search_history:
            self._vector[searched_product.lower()] = SEARCHED_PRODUCT_WEIGHT
        for followed_product in followed_products:
            self._vector[followed_product.lower()] = FOLLOWED_PRODUCT_WEIGHT
        for declined_recommendation in declined_recommendations:
            self._vector[
                declined_recommendation.lower()] = DECLINED_PRODUCT_WEIGHT

        self.rated_products = self._vector.keys()

    def __getitem__(self, item: str):
        """
        returns the rating for the given product name(item)
        or 0 if the product is not rated

        :param item: the name of the product the rating is returned for
        :return: the rating for the given product
        """
        if item in self._vector:
            return self._vector[item]
        return 0

    def __mul__(self, other: 'PreferencesVector') -> float:
        """
        performs a scalar multiplication between the two vectors

        :param other: the other vector of the multiplication
        :return: the dot product of the two vectors
        """
        result = 0.0
        for rated_product in self.rated_products:
            result += self[rated_product] * other[rated_product]

        return result

    def __repr__(self) -> str:
        """returns the representation of the vector"""
        return str(self._vector)

    @property
    def magnitude(self) -> float:
        """
        calculates the length of the vector

        :return: the length of the vector
        """
        return sqrt(self * self)

    def json(self) -> str:
        """serializes the vector to a json string"""
        return json.dumps(self._vector)
