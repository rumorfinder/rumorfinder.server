import json
import os
import signal
import socket
import struct
import subprocess
import time
from contextlib import closing
from dataclasses import dataclass
from threading import Lock
from typing import Optional, Iterator

import requests


def start(process_count: int, conn_timeout: float, rw_timeout: float):
    """
    starts the parser processes

    :param process_count: the number of parser processes to start
    :param conn_timeout: the timeout length for connecting to a parser process
    :param rw_timeout: the timeout for read/write operations to the parser
    process
    :raises TimeoutError: An error is raised if a timeout occurs
    """
    global process_queue, parser_path, connection_timeout, \
        readwrite_timeout

    # the file is in the same working directory as the request.py
    # file, so we locate the current directory and append the
    # file name to the current directory to get the full file path
    parser_path = 'node "%s\\parser.js"' % \
                  os.path.dirname(os.path.abspath(__file__))

    connection_timeout = conn_timeout
    readwrite_timeout = rw_timeout

    # create the parser processes
    process_queue = ProcessQueue()
    for i in range(process_count):
        port = _find_free_port()
        popen = _create_parser_process(port)
        lock = Lock()
        process = Process(port, popen, lock)
        process_queue.insert(process)

    # we wait for the process to be responsive before exiting the function
    for process in process_queue:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            if not _try_connect(sock, process.port):
                raise TimeoutError()


def stop():
    """
    stops the parser processes. start() must be called after for the parser
    to work again

    """
    for process in process_queue:
        process.kill()
    process_queue.clear()


def parse(url: str) -> str:
    """
    Parses the given url and returns only the content's html

    :param url: the url to parse
    :return: the content of the url in HTML format
    :raises ValueError: when the url doesn't exist
    :raises ParserException: when the parser fails
    """
    if not _url_exists(url):
        raise ValueError()

    parsed_result = _call_parser(url)
    content = parsed_result.get('content')

    if content is None:
        raise ParserException()

    return content


def _create_parser_process(port: int) -> subprocess.Popen:
    """
    runs a new process of the parser that will listen to requests on the
    given port

    :param port: the port the new parser process will listen to
    :return: the parser process object
    """
    command = ' '.join([parser_path, str(port)])
    return subprocess.Popen(command, stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            creationflags=subprocess.CREATE_NEW_PROCESS_GROUP)


def _try_connect(sock: socket.socket, port: int) -> bool:
    """
    attempts to connect to the parser at the given port

    :param sock: the socket that will eb connected to the parser
    :param port: the port the parser is listening to
    :return: true if the connection was successful, false otherwise
    """
    timeout = sock.timeout
    try:
        sock.settimeout(connection_timeout)
        t = 0.0
        while t < connection_timeout:
            try:
                sock.connect(('127.0.0.1', port))
                return True
            except OSError:
                time.sleep(1)
                t += 1
        return False
    except TimeoutError:
        return False
    finally:
        sock.settimeout(timeout)


def _receive_response(sock: socket.socket) -> Optional[str]:
    """
    reads the entire response from the parser

    :param sock: the socket connected to the parser
    :return: the data sent from the parser
    """
    # Read message length and unpack it into an integer
    packed_resp_len = _receive_all(sock, 4)
    if not packed_resp_len:
        return None
    resp_len = struct.unpack('>I', packed_resp_len)[0]

    # Read the message data
    data = _receive_all(sock, resp_len)
    if data:
        return data.decode()
    return None


def _receive_all(sock: socket.socket, n: int) -> Optional[bytes]:
    """attempts to receive an entire message of length n"""
    data = b''
    while len(data) < n:
        try:
            packet = sock.recv(n - len(data))
        except (TimeoutError, OSError):
            return None
        if not packet:
            return None
        data += packet
    return data


def _find_free_port():
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(('', 0))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return s.getsockname()[1]


def _call_parser(url: str) -> dict:
    """
    passes a parser process the given url

    :param url: the url to be passed into the parser
    :return: the parser's result
    """
    process = process_queue.take()
    process.lock.acquire()
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock.settimeout(readwrite_timeout)
        if not _try_connect(sock, process.port):
            # if for some reason we can't connect to a process, we attempt to
            # create a new process and connect to it
            port = _find_free_port()
            popen = _create_parser_process(port)
            if not _try_connect(sock, port):
                raise ParserException()

            # we kill the previous process and update the process object to
            # reflect the changes
            process.kill()
            process.port = port
            process.popen = popen

        # we send the url to the process
        try:
            sock.sendall(url.encode())
        except (TimeoutError, OSError) as e:
            raise ParserException(str(e))

        # now we receive the parsed result
        response = _receive_response(sock)
        if response is None:
            raise ParserException()
    finally:
        sock.close()
        process.lock.release()

    try:
        result = json.loads(response)
    except json.JSONDecodeError as e:
        raise ParserException(str(e))

    if 'error' in result:
        err = result.get('message')
        if not err:
            err = ''
        raise ParserException(err)

    return result


def _url_exists(url: str) -> bool:
    """Checks if the given url exists and can be loaded"""
    try:
        request = requests.head(url)
        return request.status_code < 400
    except requests.exceptions.RequestException:
        return False


class ParserException(Exception):
    """
    An exception thrown when the parser encounters an error while
    parsing a url.
    """
    pass


@dataclass()
class Process:
    """
    represents a parser process instance
    """

    port: int
    popen: subprocess.Popen
    lock: Lock

    def kill(self):
        """Kills the process"""
        self.popen.send_signal(signal.CTRL_BREAK_EVENT)


class ProcessQueue:
    """
    A thread-safe queue that continuously updates the ordering in order to
    always have the best chance at receiving a free process
    """

    def __init__(self):
        self.list_a = []
        self.list_b = []
        self.lock = Lock()

    def insert(self, item: Process):
        """
        inserts a new item into the queue

        :param item: the item to be added
        """
        self.lock.acquire()
        self.list_a.append(item)
        self.lock.release()

    def take(self) -> Optional[Process]:
        """
        returns the first item of the queue and moves it to the back of the
        queue

        :return: the top item of the queue or None if the queue is empty
        """
        self.lock.acquire()
        result = None
        if len(self.list_a) >= 0:
            result = self.list_a.pop(0)
            self.list_b.append(result)
            if len(self.list_a) == 0:
                self.list_a = self.list_b
                self.list_b = []

        self.lock.release()
        return result

    def clear(self):
        """
        clears the queue
        """

    def __iter__(self) -> Iterator[Process]:
        self.lock.acquire()
        lst = self.list_a + self.list_b
        self.lock.release()
        for item in lst:
            yield item


if __name__ == '__main__':
    pass
