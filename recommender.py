from typing import Any
from uuid import UUID

from sortedcontainers import SortedList

import db
from preferences_vector import PreferencesVector

# constants values of the recommender
MAX_NEIGHBOURS = 3
MAX_RECOMMENDATIONS = 3


def get_recommendations(user_id: UUID, user_vector: PreferencesVector):
    """
    Returns recommendations for the given user.

    :param user_id: the id of the user that recommendations will be returned
    for
    :param user_vector: the preferences vector of the given user
    :return: a list containing any found recommendations for the user,
    sorted from most likely to be liked(by the user) to least likely to be
    liked.
    """
    similarities = {}
    neighbours = SortedList(key=lambda x: similarities[x])

    # find the nearest neighbours
    for neighbour_vector in db.select_other_neighbours(user_id):
        # we use the cosine between the vectors  as our similarity function
        similarities[neighbour_vector] = _cosine(user_vector,
                                                 neighbour_vector)
        # make sure the list doesnt exceed the maximum
        deleted_item = _add_sorted(neighbours, neighbour_vector,
                                   MAX_NEIGHBOURS)
        # make sure we delete excess values from the similarities dictionary
        if deleted_item is not None:
            del similarities[deleted_item]

    # get the possible recommendations -> products the user has not rated
    possible_recommendations = set()
    for neighbour in neighbours:
        for rated_product in neighbour.rated_products:
            if rated_product not in user_vector.rated_products:
                possible_recommendations.add(rated_product)

    # initialize the data structures
    predicted_ratings = {}
    recommendations = SortedList(key=lambda x: predicted_ratings[x])

    # k is the normalising factor for the prediction
    k = 1 / sum(similarities.values())

    # predict the rating for each possible recommendation
    for possible_recommendation in possible_recommendations:
        prediction = 0
        for neighbour in neighbours:
            similarity = similarities[neighbour]
            rating = neighbour[possible_recommendation]
            prediction += similarity * rating
        # make sure to normalize the prediction
        predicted_ratings[possible_recommendation] = k * prediction
        # make sure the list doesnt exceed the maximum
        deleted_item = _add_sorted(recommendations, possible_recommendation,
                                   MAX_RECOMMENDATIONS)
        # make sure we delete excess values from the predictions dictionary
        if deleted_item is not None:
            del predicted_ratings[deleted_item]

    # we return the recommendations in reverse because the list is sorted
    # in ascending order but we want to return a list in descending order
    return recommendations[::-1]


def _add_sorted(lst: SortedList, item: Any, max_items: int) -> Any:
    """
    adds the item to the sorted list and ensures the list is in descending
    order
    and doesnt exceed the maximum length.
    if it does then the smallest element is removed.

    :param lst: the list the item will be added to
    :param item: the value to add to the list
    :param max_items: the maximum number of items the list can hold
    :return: the item deleted from the list, or None if no object was deleted
    """
    lst.add(item)
    if len(lst) > max_items:
        # we remove the first element because we want to keep the list in
        # descending order
        deleted_item = lst[0]
        del lst[0]
        return deleted_item
    return None


def _cosine(vector1: PreferencesVector, vector2: PreferencesVector) -> float:
    """
    returns cos(a) where a is the angle between the two vectors

    :param vector1:
    :param vector2:
    :return: cos(a)
    """
    return (vector1 * vector2) / (vector1.magnitude * vector2.magnitude)
