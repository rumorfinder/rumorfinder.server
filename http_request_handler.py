import json
from http.server import *
from urllib.parse import parse_qs, urlparse

from psycopg2 import Error as DB_Error

import db
import recommender
import web_parser
from request import Request

RECOMMENDATIONS_PATH = "/recommendations/"
PARSER_PATH = "/parse"

MAX_ALLOWED_URLS = 1
URL_PARAMETER_NAME = 'url'

CONTENT_LENGTH = "content-length"
CONTENT_TYPE = "content-type"
JSON_CONTENT_TYPE = "application/json"
HTML_CONTENT_TYPE = "text/html"

SUCCESS_CODE = 200
NOT_FOUND = 404
BAD_REQUEST = 400
INTERNAL_ERROR = 500

ERROR_MESSAGES = {
    NOT_FOUND: "Path Not Found",
    BAD_REQUEST: "Incorrect Request Format",
    INTERNAL_ERROR: "Internal Server Error"
}


class HTTPRequestHandler(BaseHTTPRequestHandler):
    """Represents the server's request handler"""

    def _read_incoming_data(self) -> str:
        """
        Reads any incoming data from the client

        :return: the decoded read data or None if no data was received
        """
        content_length_header = (self.headers[CONTENT_LENGTH])
        data_length = int(content_length_header) if len(
            content_length_header) > 0 else None
        data = self.rfile.read(data_length)
        return data.decode('utf-8')

    def _respond(self, payload: str, content_type: str) -> None:
        """
        Reports a successful request, sends appropriate headers and sends
        the payload to the client

        :param payload: the  data to send to the client
        :param content_type: the content type of the payload
        """
        # encode the payload
        encoded_payload = bytes(payload, 'utf-8')

        # send the response status code
        self.send_response(SUCCESS_CODE)

        # send the headers
        self.send_header(CONTENT_TYPE, content_type)
        self.send_header(CONTENT_LENGTH, str(len(encoded_payload)))
        self.end_headers()

        # send the payload
        self.wfile.write(encoded_payload)

    def do_GET(self):
        """
        This method is called to handle an incoming GET request, currently
        only performs parser requests
        """
        try:
            # parse the path because we expect url parameters
            url = urlparse(self.path)
            # ensure the path is correct
            if url.path != PARSER_PATH:
                self.send_error(NOT_FOUND, ERROR_MESSAGES[NOT_FOUND])
                return

            parameters = parse_qs(url.query)
            urls = parameters.get(URL_PARAMETER_NAME)

            # ensure the query parameters are correct
            if urls is None or len(urls) > MAX_ALLOWED_URLS:
                self.send_error(BAD_REQUEST, ERROR_MESSAGES[BAD_REQUEST])
                return

            try:
                response = web_parser.parse(urls[0])
            except ValueError:
                # value error means the url is not valid
                self.send_error(BAD_REQUEST, ERROR_MESSAGES[BAD_REQUEST])
                return
            except web_parser.ParserException:
                self.send_error(INTERNAL_ERROR, ERROR_MESSAGES[INTERNAL_ERROR])
                return

            self._respond(response, HTML_CONTENT_TYPE)
        except ConnectionAbortedError:
            pass

    def do_POST(self):
        """
        This method is called to handle an incoming POST request, currently
        only performs recommendations requests
        """
        try:
            # make sure the request path is correct
            if self.path != RECOMMENDATIONS_PATH:
                self.send_error(NOT_FOUND, ERROR_MESSAGES[NOT_FOUND])
                return

            # read the request body
            received_data = self._read_incoming_data()
            # if received data is None then there is no message body
            if received_data is None:
                self.send_error(BAD_REQUEST, ERROR_MESSAGES[BAD_REQUEST])
                return

            request = Request(received_data)
            # check the format of the request
            if not request.is_valid():
                self.send_error(BAD_REQUEST, ERROR_MESSAGES[BAD_REQUEST])
                return

            # parse the request
            id, preferences_vector = request.parse()
            try:
                # try and upsert the client's data
                db.upsert_user(id, preferences_vector)

                # retrieve the recommendations for the client
                recommendations = \
                    recommender.get_recommendations(id, preferences_vector)

                # send the response
                self._respond(json.dumps(recommendations), JSON_CONTENT_TYPE)
            except DB_Error:  # an error occurred while accessing the db
                self.send_error(INTERNAL_ERROR, ERROR_MESSAGES[INTERNAL_ERROR])
        except ConnectionAbortedError:
            pass
